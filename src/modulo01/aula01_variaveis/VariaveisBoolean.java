package modulo01.aula01_variaveis;

public class VariaveisBoolean {

	public static void main(String[] args) {
		
		boolean verdadeiro = true;
		
		boolean falso = false;

		System.out.println("O valor de verdadeiro é " + verdadeiro);
		System.out.println("O valor de false é " + falso);
	}

}
